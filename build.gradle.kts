plugins {
    kotlin("jvm") version "1.3.72"
}

repositories {
    jcenter()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))

    testImplementation("org.jdbi", "jdbi3-kotlin-sqlobject", "3.14.1")
    testImplementation("com.zaxxer", "HikariCP", "3.4.5")
    testImplementation("org.postgresql", "postgresql", "42.2.14")
    testRuntimeOnly("org.slf4j", "slf4j-simple", "1.7.30")

    testRuntimeOnly("org.junit.jupiter", "junit-jupiter-engine", "5.6.2")
    testImplementation("org.junit.jupiter", "junit-jupiter-api", "5.6.2")
}

tasks.test {
    useJUnitPlatform()
}
