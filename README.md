Reproduction of an issue with JDBI where it throws an exception when trying to use `Jdbi.inTransaction` on a `DataSource` that has auto commit disabled.

To make it semi realistic, this has a real connection pool and db.

Run the tests:

```
docker-compose up -d
./gradlew check
```
