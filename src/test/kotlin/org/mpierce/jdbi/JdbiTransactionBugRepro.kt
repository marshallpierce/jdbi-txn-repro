package org.mpierce.jdbi

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import org.jdbi.v3.core.Jdbi
import org.jdbi.v3.core.kotlin.KotlinPlugin
import org.jdbi.v3.core.transaction.TransactionIsolationLevel
import org.jdbi.v3.sqlobject.SqlObjectPlugin
import org.jdbi.v3.sqlobject.kotlin.KotlinSqlObjectPlugin
import org.junit.jupiter.api.Test

class JdbiTransactionBugRepro {
    @Test
    internal fun withAutoCommitSerializalbe() {
        jdbiFromConfig(baseConfig.apply { isAutoCommit = true }) { jdbi ->
            jdbi.inTransaction<Unit, Exception>(TransactionIsolationLevel.SERIALIZABLE) { }
        }
    }

    @Test
    internal fun withoutAutoCommitSerializable() {
        jdbiFromConfig(baseConfig.apply { isAutoCommit = false }) { jdbi ->
            // this throws
            jdbi.inTransaction<Unit, Exception>(TransactionIsolationLevel.SERIALIZABLE) { }
        }
    }

    @Test
    internal fun withAutoCommitDefaultIsolation() {
        jdbiFromConfig(baseConfig.apply { isAutoCommit = true }) { jdbi ->
            jdbi.inTransaction<Unit, Exception> { }
        }
    }

    @Test
    internal fun withoutAutoCommitDefaultIsolation() {
        jdbiFromConfig(baseConfig.apply { isAutoCommit = false }) { jdbi ->
            jdbi.inTransaction<Unit, Exception> { }
        }
    }
}

private val baseConfig = HikariConfig().apply {
    jdbcUrl = "jdbc:postgresql://localhost:11000/jdbi"
    username = "jdbi"
    password = ""
    maximumPoolSize = 2
}

private fun jdbiFromConfig(config: HikariConfig, block: (Jdbi) -> Unit) {
    HikariDataSource(config).use { ds ->
        val jdbi = Jdbi.create(ds).apply {
            installPlugin(SqlObjectPlugin())
            installPlugin(KotlinPlugin())
            installPlugin(KotlinSqlObjectPlugin())
        }
        block(jdbi)
    }
}
